package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Calculadora
 */
@WebServlet({"/Calculadora" , "/calculadora"})
public class Calculadora extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calculadora() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion = request.getSession();
		String user = (String) sesion.getAttribute("usuario");
		
		if (user == null) {			
			response.sendRedirect(request.getContextPath() + "/login"); // Redireccion
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/calculadora.jsp");
			dispatcher.forward(request, response); // Redireccion con datos
		}
		
		return;	
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("En POST");
		
		int operador1 = parseInt(request.getParameter("op1"));
		System.out.println("Operador1 " + operador1);
	
		int operador2 = parseInt(request.getParameter("op2"));
		System.out.println("Operador2 " + operador2);
		
		String operacion = request.getParameter("operacion");
		int resultado = resultado(operador1, operador2, operacion);
		
		System.out.println("El resultado es: "+ resultado);
		
		request.setAttribute("op1", operador1);
		request.setAttribute("op2", operador2);
		request.setAttribute("operacion", operacion);
		request.setAttribute("resultado", resultado);
		//request.setAttribute("error", error);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/calculadora.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private int resultado(int op1,int op2, String operacion) {
		switch(operacion){
		case "Sumar":
			return op1+op2;			
		case "Restar":
			return op1-op2;
		case "Multiplicar":
			return op1*op2;
		case "Dividir":
			return op1/op2;
		default:
			return 0;	
		}	
		
		
	}
	
	
	private int parseInt(String operador){
		try{
			int entero = Integer.parseInt(operador);
			return entero;
		}catch(NumberFormatException err){
			return 0;
		}
	}

}
