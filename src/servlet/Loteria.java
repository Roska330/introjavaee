package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Loteria
 */
@WebServlet({ "/Loteria", "/loteria" })
public class Loteria extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ArrayList<Integer> apuesta = new ArrayList<>();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Loteria() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		System.out.println(method);
		if(method != null && method.equals("apostar")) {	
			System.out.println("Voy apostar");
			apostar(request,response);	
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/loteria.jsp");
			dispatcher.forward(request, response); // Redireccion con datos
		}
		return;
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	protected void apostar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		// Recogemos la apuesta hasta el momento
				
		if(apuesta == null)
			apuesta = new ArrayList<>();
		
		// Recogemos el numero que vamos a apostar 
		Integer numero = Integer.parseInt(request.getParameter("numero"));		
		
	
		if(!apuesta.contains(numero))
			apuesta.add(numero);
		else
			apuesta.remove(numero);
		
		request.setAttribute("apuesta", apuesta);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/loteria.jsp");
		dispatcher.forward(request, response); // Redireccion con datos

		return;
		
	}

}
