package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet({ "/Login", "/login" })
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie[] cookies = request.getCookies();
		String usuario = "";
		for(Cookie c : cookies){
			if(c.getName().equals("usuario")) {
				usuario = c.getValue();
				break;
			}
		}
		request.setAttribute("usuario", usuario);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/login.jsp");
		dispatcher.forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		System.out.println("Método: " + method);
		
		if(method != null && method.equals("logout")){
			logout(request,response);
		} else {
			login(request,response);
		}
	}
	
	protected void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession sesion = request.getSession();
		sesion.removeAttribute("usuario");
		response.sendRedirect(request.getContextPath() + "/login");
		return;
		
	}
	protected void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String usuario = request.getParameter("usuario");
		if(usuario.length() > 1) {
			HttpSession sesion = request.getSession();
			sesion.setAttribute("usuario", usuario);
			Cookie cokie = new Cookie("usuario", usuario);
			response.addCookie(cokie);
			response.sendRedirect(request.getContextPath() + "/calculadora");
		} else {
			response.sendRedirect(request.getContextPath() + "/login");
		}
		return;
	}
	

}
