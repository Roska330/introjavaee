<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Calculadora con Java EE</h1>
<h2><%= session.getAttribute("usuario") %></h2>	
	<form action="" method="post">
		<label>Operador 1</label> <input type="number" name="op1" value="<%= request.getAttribute("op1") %>"> <br>
		<label>Operacion</label> 
		<select name="operacion">
			
			<option value="Sumar" <%= request.getAttribute("operacion") != null && request.getAttribute("operacion") == ("Sumar") ? "selected" : "" %> > Sumar</option>
			<option value="Restar" <%= request.getAttribute("operacion") != null && request.getAttribute("operacion").equals("Restar") ? "selected" : "" %> > Restar</option>
			<option value="Multiplicar"<%= request.getAttribute("operacion") != null && request.getAttribute("operacion").equals("Multiplicar")? "selected" : "" %> >Multiplicar</option>
			<option value="Dividir"<%= request.getAttribute("operacion") != null &&request.getAttribute("operacion").equals("Dividir") ? "selected" : "" %> >Dividir</option>
			
		</select> <br> <label>Operador 2</label> <input type="number" name="op2" value="<%= request.getAttribute("op2") %>"> <br> 
		<input type="submit" value="Calcular">
	</form>
	<form action="/introjavaee/login?method=logout" method="post">
	<input type="submit" value="Cerrar Sesión">
	</form>
	<%
		if(request.getAttribute("resultado") != null) { %>
		<h4>Resultado: <%= request.getAttribute("resultado") %></h4>
	<% } %>
	
	<%
		if(request.getAttribute("error") != null) { %>
		<h4>Error: <%= request.getAttribute("error") %></h4>
	<% } %>
	
	
</body>
</html>