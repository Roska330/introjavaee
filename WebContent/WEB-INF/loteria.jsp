<%@ page import="java.util.ArrayList"  %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Loteria Navideña</title>
</head>
<body>


	
	<table border="1" align="center">
	<%
	int numero = 1;
	ArrayList<Integer> apuesta = (ArrayList<Integer>) request.getAttribute("apuesta");	
	for(int i = 0; i < 7; i++) { 
        out.print("<tr>");  
        for(int j = 0; j < 7; j++) {    
            out.print("<td style='background-color: " + (apuesta != null && apuesta.contains(numero) ? "green" : "") +
            		"'> <a href=?method=apostar&numero=" 
        				+ numero +">" + numero++ + "</a></td>");
         } 
        out.print("<tr>"); 
     }
	%>	
	</table>
	<hr>
	<div align="center">
	<input type="submit" value="Realizar apuesta"> 
	</div>
	
	<div align="center">
	<hr>
	<%	
	
	if(apuesta != null && apuesta.size() > 0) {
		out.print("<h3> APUESTA: </h1>");
		
		for(int n : apuesta) {
			out.println("<b>" + n + "</b><br>");
		}
	}	
	 %>
	</div>

</body>
</html>